
Network - Protocol - IP - MACadress - MASK

OSI 7 Layers:		TCP/IP
    Application		Application
    Presentation	Application
    Session		Application
    Transport		Transport (TCP/UDP)
    Network		Internet
    Data Link		Network Access
    Physical		Network Access

Protocols: set of logical instruction for data managment: /etc/services - TCP/UDP - ports
	FTP - 20	FTP - 21
	SSH - 22	TELNET - 23
	SMTP - 25	DNS - 53
	HTTP - 80	POP3 - 110
	NTP - 123	NETBIOS - 139
	IMAP - 143	SNMP - 161
	SNMPTRAP, SNMP - 162
	LDAP - 389	HTTPS - 443
	SMTPS - 465	RSH - 514
	LDAPS - 636	IMAPS - 993
	POP3S - 995

Transmission Control Protocol (TCP)

TCP is a connection-oriented transport protocol. This means that a connection is established between the client through the socket port, and the service through the service standard port.


User Datagram Protocol (UDP)

UDP establishes a connection between the client and the service, but does not control the data transmission of that connection. In other words, it does not check if packages have been lost, or if they are out of order, etc. The application is responsible for implementing the controls that are necessary.


Internet Control Message Protocol (ICMP)

ICMP is a network layer protocol in the TCP/IP stack and its main function is to analyze and control network elements, making it possible, for example:

% Traffic volume control % Detection of unreachable destinations
%  Route redirection % Checking the status of remote hosts

It is the protocol used by the ping command, which will be studied in another subtopic.

https://learning.lpi.org/en/learning-materials/102-500/109/109.1/109.1_02/

IP - internet protocol - responsible for the logical addressing of a host - "each device on the network is assigned a unique IP address, and it is possible to assign more than one address to the same device." -> source/destination
 
# 128  64  32  16  8  4  2  1
#  1    1   0   0  0  0  0  1
# 128+64+1=193 --> 193.
#  1    1   1   0  1  0  0  1
# 128+64+32+8+1=233 --> 193.233
#  0    0   1   0  1  1  0  0
# 32+8+4=44 --> 193.233.44
#  0    0   0   0  1  1  0  0
# 8+4=12 --> 193.233.44.12

	193.233.44.12 --> 1100 0001.1110 1001.0010 1100.0000 1100

MAC-address: Media Access Control Address - is a hardware identification number that uniquely identifies each device on a network - 54:52:00:4d:77:d3 - network card addresses
#ifconfig -a / ip a s

Address Classes: Private
 A  - 1.0.0.0 - 126.255.255.255
 B  - 128.0.0.0 - 191.255.255.255
 C  - 192.0.0.0 - 223.255.255.255

# 127.0.0.1 - loopback - a closed system for its own self-examination - virtual address for network card
# Connection to public network --> Routers & Switches

Netmask:
The network mask (or netmask) is used in conjunction with the IP address to determine which part of the IP represents the !network and which represents the !hosts.

32-bit masks were created to be used for two main purposes:
% first, to administer internet sites and divide an IP address into what are called subnets
% second to specify the available hosts for the networks and determine where data goes when it is received by the router.
https://www.easytechjunkie.com/what-is-a-netmask.htm

#  0-255.     0-255.     0-255.     0-255
   0000 0000. 0000 0000. 0000 0000. 0000 0000
   128  64  32  16  8  4  2  1

# if we don't want to change bits in octets except for one last octet
	10.0.0.0 --> 0000 1010.0000 0000.0000 0000/.0000 0000   ---> 10.0.0.0/24 -> 254 possible client
                     1111 1111.1111 1111.1111 1111/.0000 0000
                     -"-    do not change     -"- /.' change' 
# change 2 last octets
	10.0.0.0/16 >1111 1111.1111 1111/.0000 0000.0000 0000
		     -"-do not change-"-/.'    change       '
	10.0.1.0 - 10.0.255.254


	
